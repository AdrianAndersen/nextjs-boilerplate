const HelperComponent = () => (
  <p className="p-3 rounded bg-blue-500 font-semibold mt-5 mb-5 text-center">
    I am a helper component
  </p>
);

export default HelperComponent;
